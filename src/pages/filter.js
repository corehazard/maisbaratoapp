import React, { Component, Fragment } from 'react';

import api from '../services/api';

import { View, Text, StyleSheet, TouchableHighlight, Dimensions, Platform } from 'react-native';

import { Icon, Header, Left, Body, Right } from 'native-base';

import RNPickerSelect from 'react-native-picker-select';

import Slider from '@react-native-community/slider';
// import { Container } from './styles';


export default class Filter extends Component {
  static defaultProps = {
    value: 500,
  };

  constructor(props) {
    super(props);

    this.state = {
      displayDistance: this.props.value + " Metros",
      distance: this.props.navigation.dangerouslyGetParent().getParam('distance') !== undefined
        ? this.props.navigation.dangerouslyGetParent().getParam('distance')
        : this.props.value,
      category: this.props.navigation.dangerouslyGetParent().getParam('category') !== undefined
        ? this.props.navigation.dangerouslyGetParent().getParam('category')
        : '',
      categories: [],
    }
  }

  changeDisplayDistance = (value) => {
    this.setState({
      displayDistance: value <= 1000 ? value + " Metros" : (value / 1000) + " Quilômetros",
      distance: value
    });
  }

  loadCategories = async () => {
    const response = await api.get("/products/_category");
    const { data } = response;

    const listObj = [];

    for (const { visual_name, name } of data) {
      listObj.push({ label: visual_name, key: name, value: name });
    }

    this.setState({ categories: [... this.state.categories, ...listObj] });
  };

  returnToMain = () => {
    this.props.navigation.navigate({
      routeName: 'Main',
      key: 'Main',
      params: {
        distance: this.state.distance,
        category: this.state.category
      }
    });
  }

  componentDidMount() {
    this.loadCategories();
  }

  render() {
    return (
      <Fragment>
        <Header
          style={{ backgroundColor: '#FDD912' }}
          androidStatusBarColor='#E3C310'
        >
          <Left style={{ flex: 1 }}>
            {/* <Icon name="ios-arrow-round-back" onPress={() => this.returnToMain()} /> */}
          </Left>
          <Body style={{ flex: 1, justifyContent: "center", flexDirection: "row" }}>
            <Text style={styles.HeaderTitle}>Filtros</Text>
          </Body>
          <Right style={{ flex: 1 }}></Right>
        </Header>
        <View style={styles.Container}>
          <Text style={styles.SubTitle}>Categorias</Text>
          <RNPickerSelect
            placeholder={{
              label: 'Todas as Categorias', value: '', key: 'all'
            }}
            items={this.state.categories}
            itemKey="all"
            onValueChange={(value) => {
              this.setState({
                category: value,
              });
            }}
            style={{...pickerStyles}}
            doneText="Selecionar"
          />
          <Text style={styles.SubTitle}>Localização</Text>
          <Text style={styles.TextCenter}>Raio de busca de ofertas:</Text>
          <Slider
            minimumValue={500}
            maximumValue={10000}
            minimumTrackTintColor="#FDD912"
            maximumTrackTintColor="#E3C310"
            thumbTintColor="#E3C310"
            step={500}
            value={this.state.distance}
            onValueChange={(value) => this.changeDisplayDistance(value)}
            style={styles.Slider}
          />
          <Text style={styles.TextCenter}>{this.state.displayDistance}</Text>

          <TouchableHighlight onPress={() => this.returnToMain()} style={styles.filterButton} underlayColor="#E3C310">
            <Text style={styles.filterText}>Encontrar Ofertas</Text>
          </TouchableHighlight>
        </View>
      </Fragment>);
  }
}

const dimensions = Dimensions.get('window');

const styles = StyleSheet.create({
  Container: {
    flex: 1,
    padding: 10,
    backgroundColor: "#FFF"
  },
  HeaderTitle: {
    fontWeight: "bold"
  },
  TextCenter: {
    alignSelf: "center"
  },
  SubTitle: {
    fontSize: 14,
    color: "#E3C310",
    marginTop: 5,
    marginBottom: 5,
  },
  Slider: {
    width: Platform.OS === 'ios' ? dimensions.width - 20 : dimensions.width - 10,
    height: 40,
  },
  filterButton: {
    backgroundColor: "#FDD912",
    alignSelf: 'center',
    alignItems: 'center',
    justifyContent: 'center',
    padding: 20,
    position: "absolute",
    bottom: 30,
    width: "90%",
    marginBottom: Platform.OS === "ios" ? 20 : 0,
  },
  filterText: {
    fontSize: 14,
    fontWeight: "bold",
  }
});

const pickerStyles = StyleSheet.create({
  inputIOS: {
    // borderWidth: 1.5,
    // borderRadius: 5,
    // borderColor: '#FDD912',
    padding: 10,
    justifyContent: "center",
    alignItems: "center",
    marginBottom: 20,
    fontSize: 18,
  }
});