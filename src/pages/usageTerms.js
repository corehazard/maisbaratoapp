import React, { Component, Fragment } from 'react';

import { ScrollView, Text, StyleSheet } from 'react-native';

import { Icon, Header, Left, Body, Right } from 'native-base';

// import { Container } from './styles';

export default class UsageTerms extends Component {
  static navigationOptions = {
    tabBarVisible: false,
  };

  constructor(props) {
    super(props);
  }

  returnToMain = () => {
    this.props.navigation.navigate({
      routeName: 'Main',
      key: 'Main',
    });
  }

  render() {
    return (<Fragment>
      <Header
        style={{ backgroundColor: '#FDD912' }}
        androidStatusBarColor='#E3C310'
      >
        <Left style={{ flex: 1 }}>
          <Icon name="ios-arrow-round-back" onPress={() => this.returnToMain()} />
        </Left>
        <Body style={{ flex: 1, justifyContent: "center", flexDirection: "row" }}>
          <Text style={styles.HeaderTitle}>Termos de Uso</Text>
        </Body>
        <Right style={{ flex: 1 }}></Right>
      </Header>
      <ScrollView style={styles.Container}>
        <Text style={styles.SubTitle}>
          Bem-vindo ao +Barato!
    </Text>
        <Text>
          Agradecemos por usar nossos produtos e serviços (“Serviços”).
    </Text>
        <Text>
          Ao usar nossos Serviços, você está concordando com estes termos. Leia-os com atenção.
    </Text>
        <Text>
          Nossos Serviços são muito diversos, portanto, às vezes, podem aplicar-se termos adicionais ou exigências de produtos (inclusive exigências de idade). Os termos adicionais estarão disponíveis com os Serviços relevantes e esses termos adicionais se tornarão parte de nosso contrato com você, caso você use esses Serviços.
    </Text>
        <Text style={styles.SubTitle}>
          Como usar nossos Serviços
    </Text>
        <Text>
          Não faça uso indevido de nossos Serviços. Por exemplo, não interfira com nossos Serviços nem tente acessá-los por um método diferente da interface e das instruções que fornecemos. Você pode usar nossos serviços somente conforme permitido por lei, inclusive leis e regulamentos de controle de exportação e reexportação. Podemos suspender ou deixar de fornecer nossos Serviços se você descumprir nossos termos ou políticas ou se estivermos investigando casos de suspeita de má conduta.
          O uso de nossos Serviços não lhe confere a propriedade sobre direitos de propriedade intelectual sobre os nossos Serviços ou sobre o conteúdo que você acessar. Você não pode usar conteúdos de nossos Serviços a menos que obtenha permissão do proprietário de tais conteúdos ou que o faça por algum meio permitido por lei. Estes termos não conferem a você o direito de usar quaisquer marcas ou logotipos utilizados em nossos Serviços. Não remova, oculte ou altere quaisquer avisos legais exibidos em ou junto a nossos Serviços.
          Nossos Serviços exibem alguns conteúdos que não são do grupo +Barato. Esses conteúdos são de exclusiva responsabilidade da entidade que os disponibiliza. Podemos revisar conteúdo para determinar se é ilegal ou se infringe nossas políticas, e podemos remover ou nos recusar a exibir conteúdos que razoavelmente acreditamos violar nossas políticas ou a lei. Mas isso não significa, necessariamente, que revisaremos conteúdos, portanto por favor, não presuma que o faremos.
          Em relação com seu uso dos Serviços, podemos enviar-lhe anúncios de serviços, mensagens administrativas e outras informações. Você pode desativar algumas dessas comunicações.
          Alguns dos nossos Serviços estão disponíveis em dispositivos móveis. O usuário não deve utilizar tais Serviços de forma que o distraia ou o impeça de cumprir leis de trânsito ou de segurança.
    </Text>
        <Text style={styles.SubTitle}>
          Sua Conta do +Barato
    </Text>
        <Text>
          Talvez você precise criar uma Conta do +Barato para utilizar alguns dos nossos Serviços. Você poderá criar sua própria Conta do +Barato.
          Para proteger sua Conta do +Barato, o usuário deve manter a senha em sigilo. A atividade realizada na Conta do +Barato ou por seu intermédio é de responsabilidade do usuário. Não recomendamos que a senha da Conta do +Barato seja reutilizada em aplicativos de terceiros.
          Seu Conteúdo em nossos Serviços
          Alguns de nossos Serviços permitem que você faça upload, submeta, armazene, envie ou receba conteúdo. Você mantém a propriedade de quaisquer direitos de propriedade intelectual que você detenha sobre aquele conteúdo. Em resumo, aquilo que pertence a você, permanece com você.
          Quando você faz upload, submete, armazena, envia ou recebe conteúdo a nossos Serviços ou por meio deles, você concede ao +Barato (e àqueles com quem trabalhamos) uma licença mundial para usar, hospedar, armazenar, reproduzir, modificar, criar obras derivadas (como aquelas resultantes de traduções, adaptações ou outras alterações que fazemos para que seu conteúdo funcione melhor com nossos Serviços), comunicar, publicar, executar e exibir publicamente e distribuir tal conteúdo. Os direitos que você concede nesta licença são para os fins restritos de operação, promoção e melhoria de nossos Serviços e de desenvolver novos Serviços. Essa licença perdura mesmo que você deixe de usar nossos Serviços. Alguns Serviços podem oferecer-lhe modos de acessar e remover conteúdos que foram fornecidos para aquele Serviço. Além disso, em alguns de nossos Serviços, existem termos ou configurações que restringem o escopo de nosso uso do conteúdo enviado nesses Serviços. Certifique-se de que você tem os direitos necessários para nos conceder a licença de qualquer conteúdo que você enviar a nossos Serviços.
          Nossos sistemas automatizados analisam o seu conteúdo (incluindo e-mails) para fornecer recursos de produtos pessoalmente relevantes para você, como resultados de pesquisa customizados, propagandas personalizadas e detecção de spam e malware. Essa análise ocorre à medida que o conteúdo é enviado e recebido, e quando ele é armazenado.
          Se o usuário tiver uma Conta do +Barato, o nome e a foto do perfil, bem como as ações realizadas em aplicativos do +Barato ou de terceiros que estejam conectados a essa Conta do +Barato (como marcações +1, avaliações e comentários postados), poderão aparecer em nossos Serviços, inclusive para exibição em anúncios e em outros contextos comerciais. As opções do usuário para limitar as configurações de compartilhamento ou visibilidade na Conta do +Barato serão respeitadas. Por exemplo, o usuário pode alterar as configurações de modo que seu nome e foto não apareçam em anúncios.
    </Text>
        <Text style={styles.SubTitle}>
          Sobre Software em nossos Serviços
    </Text>
        <Text>
          Quando um Serviço exige ou inclui software disponível para download, tal software poderá atualizar-se automaticamente em seu dispositivo se uma nova versão ou recurso estiver disponível. Alguns Serviços podem permitir que você ajuste suas configurações de atualizações automáticas.
          O +Barato concede a você uma licença pessoal, mundial, não exclusiva, intransferível e isenta de royalties para o uso do software fornecido pelo +Barato como parte dos Serviços. Essa licença tem como único objetivo permitir que você use e aproveite o benefício dos Serviços, tal como fornecidos pelo +Barato, da forma permitida por estes termos. Você não poderá copiar, modificar, distribuir, vender ou alugar qualquer parte de nossos Serviços ou o software incluso, nem poderá fazer engenharia reversa ou tentar extrair o código fonte desse software, exceto nos casos em que a legislação proibir tais restrições, ou quando você tiver nossa permissão por escrito.
          Software de código aberto é importante para nós. Alguns dos softwares usados em nossos Serviços podem ser oferecidos sob uma licença de código aberto que colocaremos a sua disposição. Pode haver disposições na licença de código aberto que substituam expressamente alguns desses termos.
          Como modificar e cancelar nossos Serviços
          Estamos constantemente alterando e melhorando nossos Serviços. Podemos incluir ou remover funcionalidades ou recursos e podemos também suspender ou encerrar um Serviço por completo.
          Você pode deixar de usar nossos Serviços a qualquer momento, embora fiquemos chateados ao ver você ir embora. O +Barato também poderá deixar de prestar os Serviços a você ou, incluir ou criar novos limites a nossos Serviços a qualquer momento.
          Acreditamos que você seja o proprietário dos seus dados e que é importante preservar seu acesso a esses dados. Se descontinuarmos um Serviço, quando razoavelmente possível, você será informado com antecedência razoável e terá a chance de retirar as suas informações daquele Serviço.
          Nossas Garantias e Isenções de Responsabilidade
          Fornecemos nossos Serviços usando um nível comercialmente razoável de capacidade e cuidado e esperamos que você aproveite seu uso deles. Mas existem algumas coisas que não prometemos sobre nossos Serviços.
          Exceto quando expressamente previsto nestes termos ou em termos adicionais, nem o +Barato, nem seus fornecedores ou distribuidores oferecem quaisquer garantias sobre os Serviços. Por exemplo, não nos responsabilizamos pelos conteúdos nos Serviços, por funcionalidades específicas dos Serviços, ou pela confiabilidade, disponibilidade ou capacidade de atender suas necessidades. Fornecemos os serviços “na forma em que estão”.
          Certas jurisdições prevêem de determinadas garantias, como a garantia de comerciabilidade implícita, adequação a uma finalidade específica e não violação. Na medida permitida por lei, excluímos todas as garantias.
    </Text>
        <Text style={styles.SubTitle}>
          Responsabilidade pelos nossos Serviços
    </Text>
        <Text>
          Quando permitido por lei, o +Barato e os fornecedores ou distribuidores do +Barato não serão responsáveis por perda de lucros, perda de receita, perda de dados, perdas financeiras ou por danos indiretos, especiais, consequenciais, exemplares ou punitivos.
          Na medida permitida por lei, a responsabilidade total do +Barato e de seus fornecedores e distribuidores, para qualquer reclamação sob estes termos, incluindo quaisquer garantias implícitas, limita-se ao valor que você pagou ao +Barato para usar os Serviços (ou, a nosso critério, para fornecer a você os Serviços novamente).
          Em todos os casos, o +Barato e seus fornecedores e distribuidores não serão responsáveis por qualquer perda ou dano que não seja razoavelmente previsível.
          Reconhecemos que, em alguns países, você pode ter direitos legais como consumidor. Caso você esteja usando os Serviços com objetivos pessoais, então nada nestes termos ou em quaisquer termos adicionais limitarão direitos de consumidor que não possam ser renunciados por contrato.
    </Text>
        <Text style={styles.SubTitle}>
          Sobre estes Termos
    </Text>
        <Text>
          Podemos modificar estes termos ou quaisquer termos adicionais que sejam aplicáveis a um Serviço para, por exemplo, refletir alterações da lei ou mudanças em nossos Serviços. Você deve consultar os termos regularmente. Postaremos avisos sobre modificações nesses termos nesta página. Publicaremos um aviso de alteração sobre os termos adicionais dentro do Serviço aplicável. As alterações não serão aplicadas retroativamente e entrarão em vigor pelo menos quatorze dias após sua publicação. Entretanto, alterações a respeito de novas funcionalidades de um Serviço ou alterações feitas por razões legais entrarão em vigor imediatamente. Se você não concordar com os termos alterados de um Serviço, deve descontinuar o uso desse Serviço.
          Em caso de conflito entre estes termos e os termos adicionais, os termos adicionais prevalecerão com relação a esse conflito.
          Estes termos regem a relação entre o +Barato e você. Eles não criam quaisquer direitos para terceiros.
          Caso você não cumpra estes termos e nós não tomemos providências imediatas, isso não significa que estamos renunciando a quaisquer direitos que possamos ter (como tomar providências futuras).
          Caso uma condição específica destes termos não seja executável, isso não prejudicará quaisquer outros termos.
    </Text>
      </ScrollView>
    </Fragment>);
  }
}

const styles = StyleSheet.create({
  Container: {
    flex: 1,
    padding: 5
  },
  HeaderTitle: {
    fontWeight: "bold"
  },
  TextCenter: {
    alignSelf: "center"
  },
  SubTitle: {
    fontSize: 14,
    color: "#E3C310",
    marginTop: 5,
    marginBottom: 5,
  },
});