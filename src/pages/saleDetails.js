import React, { Component, Fragment } from 'react';

import { imagesUrl } from '../../app.json';

import { View, ScrollView, Text, StyleSheet, Dimensions, Image, TouchableHighlight, Platform, Linking } from 'react-native';

import { Icon, Header, Left, Body, Right } from 'native-base';

export default class SaleDetails extends Component {
  constructor(props) {
    super(props);

    this.state = {
      sale: this.props.navigation.getParam('sale'),
      place: this.props.navigation.getParam('place'),
    };
  };

  openGps = () => {
    const { coordinates } = this.state.place.location;
    const { name } = this.state.place;

    const scheme = Platform.select({ ios: 'maps:0,0?q=', android: 'geo:0,0?q=' });
    const latLng = `${coordinates[0]},${coordinates[1]}`;
    const label = name;
    const url = Platform.select({
      ios: `${scheme}${label}@${latLng}`,
      android: `${scheme}${latLng}(${label})`
    });

    Linking.openURL(url);
  }

  render() {
    const { sale } = this.state;
    const { place } = this.state;

    return (
      <Fragment>
        <Header
          style={{ backgroundColor: '#FDD912' }}
          androidStatusBarColor='#E3C310'
        >
          <Left style={{ flex: 1 }}>
            <Icon name="ios-arrow-round-back" onPress={() => { this.props.navigation.navigate("Main") }} />
          </Left>
          <Body style={{ flex: 1, justifyContent: "center", flexDirection: "row" }}>
            <Text style={styles.PlaceTitle}>{place.name}</Text>
          </Body>
          <Right style={{ flex: 1 }}></Right>
        </Header>
        <ScrollView style={styles.Container}>
          <View style={styles.SaleDetail}>
            <Image
              loadingIndicatorSource={require('../../assets/MaisBaratoIconYellow.gif')}
              source={{ uri: imagesUrl + 'sales/' + sale.images[0] }}
              style={styles.SaleImage}
            />
            <Text style={styles.SaleName}>{sale.name}</Text>
          </View>
          <View style={styles.SaleDescription}>
            <Text style={styles.SaleDescTitle}>Descrição da Oferta:</Text>
            <Text style={styles.SaleDescText}>{sale.description}</Text>
          </View>
          <View style={styles.BottomView}>
            <TouchableHighlight style={styles.ViewOnGps} onPress={() => { this.openGps() }} underlayColor="#6BB307">
              <Text style={{ color: "#FFF", fontWeight: "bold", fontSize: 16 }}>Ver no Mapa</Text>
            </TouchableHighlight>
          </View>
        </ScrollView>
      </Fragment>
    );
  }
}

const dimensions = Dimensions.get('window');
const imageHeight = Math.round(dimensions.width * 9 / 16) + 50;
const imageWidth = dimensions.width;

const styles = StyleSheet.create({
  PlaceTitle: {
    fontWeight: "bold"
  },
  Container: {
    flex: 1,
    height: "100%",
    backgroundColor: "#FFF"
  },
  TextCenter: {
    alignSelf: "center"
  },
  SaleDetail: {
    borderBottomWidth: 1,
    borderBottomColor: '#DDD',
    padding: 10,
  },
  SaleName: {
    alignSelf: "center",
    fontSize: 20,
    fontWeight: "bold",
    marginTop: 5,
  },
  SaleImage: {
    height: imageHeight,
    width: imageWidth,
    resizeMode: "contain",
    alignSelf: "center",
    marginTop: 5,
  },
  SaleDescription: {
    flex: 1,
    padding: 10
  },
  SaleDescTitle: {
    fontSize: 16
  },
  SaleDescText: {
    fontSize: 14
  },
  ViewOnGps: {
    alignSelf: "center",
    justifyContent: "center",
    backgroundColor: "#7ACC09",
    padding: 20,
    width: "100%",
    marginBottom: Platform.OS === "ios" ? 20 : 0,
  },
  BottomView: {
    flex: 1,
    backgroundColor: "#FFF",
    justifyContent: 'flex-end',
    padding: 5,
  }
});