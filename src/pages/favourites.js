import React, { Component } from 'react';

import { View, Text, StyleSheet } from 'react-native';

// import { Container } from './styles';

export default class Favourites extends Component {
  render() {
    return (<View style={styles.Container}>
      <Text style={styles.TextCenter}>Página de Favoritos</Text>
    </View>);
  }
}

const styles = StyleSheet.create({
  Container: {
    flex: 1,
    justifyContent: "center",
    alignItems: "center",
  },
  TextCenter: {
    alignSelf: "center"
  }
});