import React, { Component, Fragment } from 'react';
import api from '../services/api';
import pnp from '../services/pnp';

import { imagesUrl } from '../../app.json';

import { View, Text, Image, TouchableHighlight, TouchableWithoutFeedback, ActivityIndicator, RefreshControl, Dimensions, StyleSheet, Platform } from 'react-native';
import { FlatList } from 'react-navigation';
import { DrawerActions } from 'react-navigation-drawer';
import { Container, Header, Content, Card, CardItem, Thumbnail, Button, Icon, Left, Body, Right, Title, Segment } from 'native-base';
import TimeAgo from 'react-native-timeago';
import Geolocation from '@react-native-community/geolocation';

let moment = require('moment'); //load moment module to set local language
require('moment/locale/pt-br'); //for import moment local language file during the application build
moment.locale('pt-br');//set moment local language to zh-cn

export default class Main extends Component {
  constructor(props) {
    super(props);

    this.state = {
      sales: [],
      places: new Map(),
      productInfo: {},
      region: {
        latitude: 0,
        longitude: 0
      },
      distance: this.props.navigation.getParam('distance', 500),
      category: this.props.navigation.getParam('category', ''),
      hasSales: false,
      firstLoad: false,
      loading: false,
      isFetching: true,
      newFilter: false,
      page: 1,
    };

    props.navigation.setParams({ main: true });
  };

  static getDerivedStateFromProps(nextProps, state) {
    if (nextProps.navigation.getParam('distance') !== state.distance ||
      nextProps.navigation.getParam('category') !== state.category) {
      return {
        distance: nextProps.navigation.getParam('distance', 500),
        category: nextProps.navigation.getParam('category', ''),
        isFetching: true,
        newFilter: true,
      }
    }

    return null;
  }

  async componentDidUpdate() {
    if (this.state.newFilter === true) {
      this.loadSales(1);
    }
  }

  async componentDidMount() {
    Geolocation.watchPosition(
      ({ coords: { latitude, longitude } }) => {
        console.log("Hey I'm watching position")
        this.setState({
          region: {
            latitude,
            longitude,
            latitudeDelta: 0.0043,
            longitudeDelta: 0.0034
          },
          isFetching: true,
        });

        this.loadPlaces();
        this.loadSales(1);
      },
      (error) => { console.log(error) },
      {
        timeout: 20000,
        enableHighAccuracy: true,
        maximumAge: 1000
      }
    );

    if (this.state.region.latitude === 0 && this.state.region.longitude === 0) {
      
      Geolocation.getCurrentPosition(
        ({ coords: { latitude, longitude } }) => {
          this.setState({
            region: {
              latitude,
              longitude,
              latitudeDelta: 0.0043,
              longitudeDelta: 0.0034
            },
            isFetching: true,
            firstLoad: true,
          });

          this.loadPlaces();
          this.loadSales(1);
        },
        error => { console.log("I'm here on error",error) },
        {
          enableHighAccuracy: true,
        }
      );
    }
  };

  loadPlaces = async () => {
    // TODO: Get nearest places
    const response = await pnp.get("/places/_allPlaces");
    const { data } = response;

    const map = new Map();

    for (const { id, name, location, image } of data) {
      map.set(id, { name, location, image });
    }

    this.setState({
      places: map
    });
  };

  loadSales = async (page = 1) => {
    // TODO: Get nearest sales
    const requestData = {
      location: {
        coordinates: [this.state.region.latitude, this.state.region.longitude],
        type: "Point"
      },
      range: this.state.distance,
      categories: this.state.category === '' ? null : [this.state.category],
      page: page
    };

    const response = await api.post("/products/_nearSales", requestData);

    console.log(requestData);

    const { sales, ...productInfo } = response.data;

    this.setState({
      sales: page === 1 ? sales : [... this.state.sales, ...sales],
      productInfo,
      page,
      hasSales: sales.length > 0,
      loading: sales.length > 0 && !this.state.firstLoad,
      isFetching: false,
      newFilter: false
    });

    this.props.navigation.setParams({ distance: this.state.distance, category: this.state.category });
  };

  showSaleDetail = (sale) => {
    this.props.navigation.navigate('SaleDetails', {
      sale: sale,
      place: this.state.places.get(sale.place_id),
    });
  };

  renderSale = ({ item }) => {
    return (
      <Card>
        <TouchableHighlight onPress={() => this.showSaleDetail(item)}>
          <Fragment>
            <CardItem>
              <Left>
                <Thumbnail source={this.state.places.get(item.place_id) != null && this.state.places.get(item.place_id).image != null ? { uri: imagesUrl + 'profiles/' + this.state.places.get(item.place_id).image } : require("../../assets/DefaultStore.png")} />
                <Body>
                  <Text style={styles.PlaceTitle}>{this.state.places.get(item.place_id) != null ? this.state.places.get(item.place_id).name : ""}</Text>
                  <Text style={styles.SaleTitle}>{item.name}</Text>
                </Body>
              </Left>
            </CardItem>
            <CardItem cardBody>
              <Image
                loadingIndicatorSource={require('../../assets/MaisBaratoIconYellow.gif')}
                source={{ uri: imagesUrl + "sales/" + item.images[0] }}
                style={styles.SaleImage}
              />
            </CardItem>
          </Fragment>
        </TouchableHighlight>
        <CardItem>
          <Left>
            {/* <Button transparent>
              <Icon type="FontAwesome" name="ticket" />
              <Text> 0</Text>
            </Button> */}
          </Left>
          <Body>
            {/* <Button transparent style={{ justifyContent: "flex-start", alignSelf: 'center', }}>
              <Icon name="chatbubbles" />
              <Text> 0</Text>
            </Button> */}
          </Body>
          <Right>
            <Button transparent style={{ justifyContent: "flex-start", alignSelf: 'center' }}>
              <Icon type="Ionicons" name="md-time" />
              <Text> </Text>
              <TimeAgo time={item.timestamp} hideAgo={true} />
            </Button>
          </Right>
        </CardItem>
      </Card>
    )
  };

  renderEmptyList = () => {
    return ! this.state.hasSales && ! this.state.isFetching ? (
      <View style={styles.EmptyList}>
        <Text style={styles.RadiustText}>
          Não há ofertas num raio de
          {this.state.distance <= 1000 ? ` ${this.state.distance} metros ` : ` ${(this.state.distance / 1000)}Km `}
          :(
        </Text>
        <Text style={styles.UsefilterText}>Clique em "Filtros" e encontre descontos!</Text>
      </View>
    ) : null
  };

  loadMore = () => {
    const { page, productInfo } = this.state;

    if (page === productInfo.pages) {
      this.setState({
        loading: false,
      });

      return;
    }

    const pageNumber = page + 1;

    this.loadSales(pageNumber);
  };

  scrollTop = () => {
    this.listRef.scrollToOffset({ offset: 0, animated: true });

    this.setState({
      isFetching: true
    }, () => {
      this.loadSales(1);
    });
  }

  render() {
    return (<Fragment>
      <Header
        style={{ backgroundColor: '#FDD912' }}
        androidStatusBarColor='#E3C310'
      >
        <Left style={styles.headerLeft}>
          <Icon name="ios-menu" onPress={() => { this.props.navigation.openDrawer() }} />
        </Left>
        <Body style={styles.headerBody}>
          <TouchableWithoutFeedback onPress={() => { this.scrollTop() }}>
            <View style={styles.topButton}>
              <Text style={styles.headerText}>+Barato</Text>
            </View>
          </TouchableWithoutFeedback>
        </Body>
        <Right style={styles.headerRight}></Right>
      </Header>
      <View style={styles.Container}>
        <FlatList
          data={this.state.sales}
          keyExtractor={item => item.id}
          contentContainerStyle={[{ flexGrow: 1 }, this.state.sales.length ? null : { justifyContent: 'center' }]}
          renderItem={this.renderSale}
          onEndReached={this.loadMore}
          onEndReachedThreshold={0.3}
          ListEmptyComponent={this.renderEmptyList}
          ListFooterComponent={(
            this.state.loading && this.state.sales.length
              ? <ActivityIndicator style={styles.Loading} size="large" color="#FDD912" animating={this.state.loading} />
              : null
          )}
          ref={(ref) => { this.listRef = ref; }}
          extraData={this.state.distance + this.state.category}
          refreshControl={<RefreshControl
            colors={["#FDD912", "#E3C310"]}
            refreshing={this.state.isFetching}
            onRefresh={() => { this.setState({ isFetching: true }, function () { this.loadSales(1) }); }}
          />
          }
        />
      </View>
    </Fragment>);
  }
}

const dimensions = Dimensions.get('window');
const imageHeight = Math.round((dimensions.width * 9 / 16) + 50);
const imageWidth = dimensions.width;

const styles = StyleSheet.create({
  Container: {
    flex: 1,
    flexGrow: 1,
    backgroundColor: '#FFF'
  },
  SaleImage: {
    height: imageHeight,
    width: imageWidth,
    resizeMode: "contain"
  },
  ProfileImage: {
    height: 50,
    width: 50,
    resizeMode: "cover",
    borderRadius: 50,
  },
  PlaceTitle: {
    fontWeight: "bold",
    fontSize: 16,
  },
  SaleTitle: {
    fontSize: 14,
  },
  SaleContainer: {
    padding: 20,
    elevation: 5,
    shadowColor: "#000",
    shadowOpacity: 0.1,
    shadowOffset: { x: 0, y: 0 },
    shadowRadius: 15,
    borderWidth: 1,
    borderColor: "#DDD",
  },
  FlatList: {
    flex: 1,
    flexGrow: 1,
    justifyContent: 'center',
  },
  EmptyList: {
    flex: 1,
    flexGrow: 1,
    alignItems: 'center',
    justifyContent: 'center',
    alignSelf: "center",
    backgroundColor: '#FFF'
  },
  RadiustText: {
    fontSize: 14,
    alignSelf: "center",
    justifyContent: "center",
    alignItems: "center",
  },
  UsefilterText: {
    alignSelf: "center",
    justifyContent: "center",
    alignItems: "center",
    fontWeight: "bold",
    // color: "#FDD912",
    fontSize: 16,
  },
  Loading: {
    marginTop: 10,
    marginBottom: 10
  },
  headerLeft: {
    flex: 1
  },
  headerBody: {
    flex: 4,
    justifyContent: "center",
    flexDirection: "row",
  },
  headerRight: {
    flex: 1,
  },
  headerText: {
    fontFamily: "Quantify",
    fontSize: Platform.OS === 'ios' ? 28 : 32,
    color: "#201E1E"
  },
  topButton: {
    flex: 1,
    alignSelf: 'center',
    alignItems: 'center',
    justifyContent: 'center',
    padding: 10,
  },
});