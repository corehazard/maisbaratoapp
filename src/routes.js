import React from 'react';

import { Text, TouchableOpacity, View, SafeAreaView, ScrollView, StyleSheet, BackHandler, Linking } from 'react-native';
import { createAppContainer } from 'react-navigation';
import { createStackNavigator } from 'react-navigation-stack';
import { createDrawerNavigator, DrawerItems } from 'react-navigation-drawer';
import { createMaterialBottomTabNavigator } from 'react-navigation-material-bottom-tabs';

import Ionicon from 'react-native-vector-icons/Ionicons';
import FaIcon from 'react-native-vector-icons/FontAwesome5';

import Main from './pages/main';
import SaleDetails from './pages/saleDetails';
import Filter from './pages/filter';
import UsageTerms from './pages/usageTerms';

import { version } from '../app.json';

const salesScreen = createStackNavigator({
  Main: {
    screen: Main,
    key: Main
  },
  SaleDetails: {
    screen: SaleDetails,
  },
  UsageTerms: {
    screen: UsageTerms
  },
}, {
  initialRouteName: 'Main',
  initialRouteKey: 'Main',
  headerMode: 'none',
  defaultNavigationOptions: {
    headerVisible: false,
  }
}
);

const mainScreen = createMaterialBottomTabNavigator({
  Home: {
    screen: salesScreen,
    navigationOptions: {
      tabBarLabel: "Início",
      tabBarIcon: ({ tintColor }) => <Ionicon name="ios-home" color={tintColor} size={18} />
    }
  },
  Filters: {
    screen: Filter,
    navigationOptions: {
      tabBarLabel: "Filtros",
      tabBarIcon: ({ tintColor }) => <FaIcon name="map-marker-alt" color={tintColor} size={18} />,
      tabBarVisible: false
    }
  },
  // Favourites: {
  //   screen: Favourite,
  //   navigationOptions: {
  //     tabBarLabel: "Favoritos",
  //     tabBarIcon: ({ tintColor }) => <Ionicon name="ios-heart" color={tintColor} size={18} />
  //   }
  // },
}, {
  initialRouteName: 'Home',
  activeColor: '#201E1E',
  inactiveColor: '#7D6C09',
  barStyle: { backgroundColor: '#FDD912' }
});

const CustomDrawerComponent = (props) => (
  <SafeAreaView style={{ flex: 1 }}>
    <View style={styles.DrawerHeader}>
      <Text style={styles.HeaderText}>+Barato</Text>
    </View>
    <ScrollView contentContainerStyle={{ flex: 1, flexDirection: 'column', justifyContent: 'space-between' }}>
      <DrawerItems {...props} />
    </ScrollView>
    <View
      style={{
        borderBottomColor: '#E5E5E5',
        borderBottomWidth: 1,
      }}
    />
    <TouchableOpacity style={styles.ItemButton} onPress={() => { Linking.openURL(`mailto:maisbaratosim@gmail.com?subject=Ajuda com App +barato - Versão ${version}`) }}>
      <Text style={styles.ItemText}>Ajuda e Contato</Text>
    </TouchableOpacity>
    <TouchableOpacity style={styles.ItemButton} onPress={() => { props.navigation.navigate("UsageTerms") }}>
      <Text style={styles.ItemText}>Termos de Uso</Text>
    </TouchableOpacity>
    <TouchableOpacity style={styles.ItemButton} onPress={() => { Linking.openURL("https://instagram.com/maisbaratosim") }}>
      <FaIcon name="instagram" style={styles.ItemText} size={22} />
      <Text style={styles.ItemText}> Seguir no Instagram</Text>
    </TouchableOpacity>
  </SafeAreaView>
)

const MainNavigtor = createDrawerNavigator({
  Home: {
    screen: mainScreen,
    navigationOptions: {
      title: "Início",
      // drawerLockMode: 'locked-closed',
      edgeWidth: 0
    },
  }
}, {
  defaultNavigationOptions: {
    headerStyle: {
      backgroundColor: "#FDD912",
    },
    headerTintColor: "#FFF",
  },
  transitionConfig: (nav) => handleCustomTransition(nav),
  contentComponent: CustomDrawerComponent,
  contentOptions: {
    activeTintColor: "#201E1E",
  }
});

const styles = StyleSheet.create({
  DrawerHeader: {
    height: 70,
    backgroundColor: "#FDD912",
    alignItems: "center",
    justifyContent: "center"
  },
  HeaderText: {
    fontFamily: "Quantify",
    fontSize: 32,
    color: "#201E1E"
  },
  ItemButton: {
    flexDirection: 'row',
    padding: 10,
    alignItems: "center",
    height: 50,
  },
  ItemText: {
    // fontWeight: "bold",
    color: "#201E1E"
  }
});

const prevGetStateForAction = mainScreen.router.getStateForAction;

mainScreen.router.getStateForAction = (action, state) => {
  // Do not allow to go back from Home
  if (action.type === 'Navigation/BACK' && state) {
    // TODO: Fix this to not close on all non bottom navigation screen
    // if (state.routes[state.index].routeName === 'Home') {
    //   BackHandler.exitApp();
    // }

    return null;
  }

  return prevGetStateForAction(action, state);
};

export default createAppContainer(MainNavigtor);