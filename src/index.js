import React, { Component } from 'react';

import { View, PermissionsAndroid, Platform } from 'react-native';
import './config/StatusBarConfig';

import Routes from './routes';

export default class App extends Component {
  componentDidMount() {
    this._askForLocationServices();
  }

  _askForLocationServices = async () => {
    if (Platform.OS === "android") {
      try {
        const granted = await PermissionsAndroid.request(
          PermissionsAndroid.PERMISSIONS.ACCESS_FINE_LOCATION,
          {
            title: 'MaisBarato Acesso ao Mapas',
            message:
              'Você deve aceitar o uso dos mapas ' +
              'para o aplicativo funcionar normalmente.',
            buttonNegative: 'Não autorizar',
            buttonPositive: 'Autorizar',
          },
        );

        if (granted === PermissionsAndroid.RESULTS.GRANTED) {
          console.log("Maps Allowed.");
        } else {
          console.log('Maps permission denied');
        }
      } catch (err) {
        console.log(err);
        return false;
      }
    }
  }

  render() {
    return <Routes />;
  }
}