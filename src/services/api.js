import axios from 'axios';
import {apiBaseUrl} from '../../app.json';

const api = axios.create({
    baseURL: apiBaseUrl
});

export default api;