import axios from 'axios';
import {pnpBaseUrl} from '../../app.json';

const pnp = axios.create({
    baseURL: pnpBaseUrl
});

export default pnp;